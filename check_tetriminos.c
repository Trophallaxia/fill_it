/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tetriminos.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 17:56:01 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/09 13:35:35 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		check_around(char **tab, int x, int y, int i)
{
	int		connex;

	connex = 0;
	if (x - 1 >= 0)
		connex += tab[y][x - 1] == '#' ? 1 : 0;
	if (tab[y][x + 1])
		connex += tab[y][x + 1] == '#' ? 1 : 0;
	if ((y - 1) >= i)
	{
		connex += tab[y - 1][x] == '#' ? 1 : 0;
	}
	if (y + 1 < i + 4)
		connex += tab[y + 1][x] == '#' ? 1 : 0;
	return (connex);
}

int		is_valid_tetrimino(char **tab, int i)
{
	int		x;
	int		y;
	int		connex;

	y = i;
	connex = 0;
	while (y < i + 4 && tab[y])
	{
		x = 0;
		while (tab[y][x])
		{
			if (tab[y][x] == '#')
				connex += check_around(tab, x, y, i);
			x++;
		}
		y++;
	}
	return ((connex > 5) ? 1 : 0);
}

int		check_tetriminos(char *str)
{
	char	**tab;
	int		i;
	int		error;

	tab = ft_strsplit(str, '\n');
	i = 0;
	error = 0;
	while (tab[i] && error == 0)
	{
		if (!(is_valid_tetrimino(tab, i)))
			error = 1;
		i += 4;
	}
	ft_strtabdel(tab);
	return (error == 0 ? 0 : 1);
}
