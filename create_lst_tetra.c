/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_tetra.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sel-ahma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 15:11:35 by sel-ahma          #+#    #+#             */
/*   Updated: 2018/12/13 14:28:06 by sel-ahma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

t_tetri		*ft_lst_new_tetra(int size)
{
	t_tetri	*create;
	t_tetri	*new;

	if (!(create = malloc(sizeof(*create))))
		return (NULL);
	create->next = NULL;
	if (size > 1)
	{
		while (--size)
		{
			if (!(new = malloc(sizeof(*new))))
				return (NULL);
			new->next = create;
			create = new;
		}
	}
	return (create);
}

t_struct	*ft_new_struct(void)
{
	t_struct	*create;

	if (!(create = (t_struct *)malloc(sizeof(*create))))
		return (NULL);
	return (create);
}

void		ft_fill_coordinates(t_tetri *tmp, char **t, t_struct *l, int occu)
{
	if (t[l->r][l->c] >= 'A' && t[l->r][l->c] <= 'Y' && l->cp == 0)
	{
		tmp->x[0] = l->c;
		tmp->y[0] = l->r - (4 * occu);
		tmp->letter = t[l->r][l->c];
		l->cp++;
	}
	if (t[l->r][l->c] >= 'A' && t[l->r][l->c] <= 'Y')
	{
		tmp->x[l->cp] = l->c - tmp->x[0];
		tmp->y[l->cp] = l->r - (4 * occu) - tmp->y[0];
	}
}

t_tetri		*create_tetra(t_tetri *tmp, char **t, int size, int occu)
{
	t_struct	*l;

	l = ft_new_struct();
	while (l->r < size * 4)
	{
		l->c = -1;
		while (l->c++ < 4)
		{
			ft_fill_coordinates(tmp, t, l, occu);
			if (t[l->r][l->c] >= 'A' && t[l->r][l->c] <= 'Y' && l->cp == 0)
				l->cp++;
			ft_fill_coordinates(tmp, t, l, occu);
			((t[l->r][l->c] >= 'A' && t[l->r][l->c] <= 'Y') ? l->cp++ : 0);
		}
		if (l->r % 4 == 3)
		{
			tmp = tmp->next;
			occu++;
			l->cp = 0;
		}
		l->r++;
	}
	free(l);
	return (tmp);
}
