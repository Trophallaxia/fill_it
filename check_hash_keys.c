/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkconnections.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sel-ahma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 18:04:00 by sel-ahma          #+#    #+#             */
/*   Updated: 2018/12/11 20:58:27 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		check_hash_keys(char **tab, int cpt)
{
	int		x;
	int		y;

	y = 0;
	while (tab[y])
	{
		x = 0;
		while (tab[y][x])
		{
			if (tab[y][x++] == '#')
				cpt++;
		}
		if (y++ % 4 == 3)
		{
			if (cpt != 4)
			{
				ft_strtabdel(tab);
				return (1);
			}
			cpt = 0;
		}
	}
	ft_strtabdel(tab);
	return (0);
}
