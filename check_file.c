/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 11:08:27 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/11 21:01:28 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int	check_file(char *str)
{
	int	error;

	error = 0;
	error += check_filepattern_count_blocks(str) < 0 ? 1 : 0;
	error += check_hash_keys(ft_strsplit(str, '\n'), 0);
	if (!error)
		error += check_tetriminos(str);
	return (error);
}
