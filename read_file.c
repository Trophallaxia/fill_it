/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 19:26:52 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/09 14:56:52 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

char	*read_file(char *file)
{
	int		fd;
	int		ret;
	char	*str;
	char	*tmp;

	str = ft_strnew(546);
	fd = open(file, O_RDONLY);
	ret = read(fd, str, 600);
	if (ret > 545 || ret < 0)
	{
		ft_strdel(&str);
		return (NULL);
	}
	tmp = str;
	str = ft_strdup(str);
	ft_strdel(&tmp);
	close(fd);
	return (str);
}
