# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sel-ahma <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/10 12:38:28 by sel-ahma          #+#    #+#              #
#    Updated: 2018/12/13 14:29:11 by sel-ahma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = fillit

SRCS =	check_file.c \
		check_filepattern_count_blocks.c \
		check_hash_keys.c \
		check_tetriminos.c \
		create_lst_tetra.c \
		fill_it.c \
		lst_del.c \
		main.c \
		map_edit.c \
		modif_file.c \
		read_file.c \
		ft_putendl.c \
		ft_strdup.c \
		ft_strlen.c \
		ft_strnew.c \
		ft_strsplit.c \
		ft_strtabdel.c \
		ft_strdel.c \
		ft_putnbr.c \
		ft_putstr.c

OBJECTS = $(SRCS:.c=.o)

HEADER = fill_it.h

CC = cc
CFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJECTS) $(HEADER)
	$(CC) -o $(NAME) $(OBJECTS)

clean:
	/bin/rm -f $(OBJECTS)

fclean: clean
	/bin/rm -f $(NAME)

re : fclean all

.PHONY: all clean fclean re
