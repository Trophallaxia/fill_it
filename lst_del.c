/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_del.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sel-ahma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 19:38:35 by sel-ahma          #+#    #+#             */
/*   Updated: 2018/12/09 17:53:03 by sel-ahma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void	del_lst_tetri(t_tetri *head)
{
	t_tetri *tmp;

	tmp = head;
	while (tmp->next)
	{
		free(tmp);
		tmp = tmp->next;
	}
	head = NULL;
}
