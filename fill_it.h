/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_it.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 17:56:47 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/13 14:22:02 by sel-ahma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILL_IT_H
# define FILL_IT_H

# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include "libft.h"

typedef struct		s_tetri
{
	char			letter;
	int				x[5];
	int				y[5];
	struct s_tetri	*next;
}					t_tetri;

typedef struct		s_stuct{
	int		r;
	int		c;
	int		cp;
}					t_struct;

int					ft_check_validity_file(char *file, int nb_blocks, int x);
int					check_tetriminos(char *str);
int					check_around(char **tab, int x, int y, int i);
int					check_hash_keys(char **tab, int cpt);
int					check_file(char *str);
t_tetri				*ft_lst_new_tetra(int size);
t_struct			*ft_new_product(t_struct *create);
void				ft_fill_coordinates(t_tetri *tmp,
											char **t, t_struct *l, int occu);
t_tetri				*create_tetra(t_tetri *head, char **t, int size, int occu);
char				**creat_map(char **map, int size);
char				**create_map(char **map, int size);
void				erase_tetri(char **map, t_tetri *tetri, int size);
void				erase_tetri(char **map, t_tetri *tetri, int size);
int					check_filepattern_count_blocks(char *str);
void				fill_it(char **tab, int nb_tetri);
int					fill_map(char **map,
								t_tetri *tetri, int size, t_tetri *begin_list);
void				ft_strtabdel(char **tab);
int					good_pos(char **map, t_tetri *tetri, int x, int y);
int					initialize_it(t_tetri *begin_list, int nb_tetri);
int					init_size(int nb);
int					is_valid_tetrimino(char **tab, int i);
char				**modif_file(char **split, char *str);
void				print_and_free(char **map, int size, t_tetri *begin_list);
void				place_tetri(char **map, t_tetri *tetri, int x, int y);
void				place_tetri(char **map, t_tetri *tetri, int x, int y);
char				*read_file(char *file);
void				del_lst_tetri(t_tetri *head);

#endif
