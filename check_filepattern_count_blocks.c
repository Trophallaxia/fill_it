/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_filepattern_count_blocks.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 07:42:43 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/09 11:24:43 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		check_line(char *str)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (str[i] != '.' && str[i] != '#')
			return (1);
		i++;
	}
	if (str[i] != '\n')
		return (1);
	return (0);
}

int		check_four_lines(char *str)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (!check_line(&str[i]) && j < 4)
	{
		i += 5;
		j++;
	}
	if (j < 4)
		return (1);
	return (0);
}

int		check_filepattern_count_blocks(char *str)
{
	int	i;
	int	nb_blocks;

	i = 0;
	nb_blocks = 0;
	if (!str[i])
		return (-1);
	while (str[i])
	{
		if (check_four_lines(&str[i]))
			return (-1);
		nb_blocks++;
		i += 20;
		if (str[i] == 0)
			return (nb_blocks);
		if (!(str[i] == '\n' && !check_four_lines(&str[i + 1])))
			return (-1);
		i++;
	}
	return (nb_blocks);
}
