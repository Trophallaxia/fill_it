/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_it.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/09 09:22:20 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/11 20:42:07 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		good_pos(char **map, t_tetri *tetri, int x, int y)
{
	int	i;
	int	c;
	int	l;

	i = 1;
	while (i < 5)
	{
		c = tetri->x[i];
		l = tetri->y[i];
		if (x + c < 0 || y + l < 0)
			return (0);
		if (map[y + l][x + c] != '.')
			return (0);
		i++;
	}
	return (1);
}

int		fill_map(char **map, t_tetri *tetri, int size, t_tetri *begin_list)
{
	int		x;
	int		y;

	y = 0;
	if (tetri == NULL)
		print_and_free(map, size, begin_list);
	while (map[y][0])
	{
		x = 0;
		while (map[y][x])
		{
			if (good_pos(map, tetri, x, y))
			{
				place_tetri(map, tetri, x, y);
				fill_map(map, tetri->next, size, begin_list);
				erase_tetri(map, tetri, size);
			}
			x++;
		}
		y++;
	}
	return (0);
}

int		initialize_it(t_tetri *begin_list, int nb_tetri)
{
	char	**map;
	int		size;
	int		filled;
	int		i;

	filled = 0;
	i = 0;
	size = init_size(nb_tetri);
	while (filled == 0)
	{
		if (!(map = create_map(map, size)))
			return (0);
		filled = fill_map(map, begin_list, size, begin_list);
		while (i <= size)
		{
			ft_strdel(&map[i]);
			i++;
		}
		free(map);
		size++;
	}
	return (1);
}

void	fill_it(char **tab_tetri, int nb_tetri)
{
	t_tetri *begin_list;
	t_tetri *tmp;

	begin_list = ft_lst_new_tetra(nb_tetri);
	tmp = begin_list;
	tmp = create_tetra(tmp, tab_tetri, nb_tetri, 0);
	ft_strtabdel(tab_tetri);
	initialize_it(begin_list, nb_tetri);
}
