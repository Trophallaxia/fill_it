/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 19:00:00 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/11 18:51:56 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		init_size(int nb)
{
	int	size;

	size = 2;
	while (size * size < nb * 4)
		size++;
	return (size);
}

char	**create_map(char **map, int size)
{
	int i;
	int j;

	i = 0;
	j = 0;
	map = (char**)malloc(sizeof(*map) * (size + 1));
	while (i < size)
	{
		map[i] = ft_strnew(size);
		j = 0;
		while (j < size)
		{
			map[i][j] = '.';
			j++;
		}
		i++;
	}
	map[i] = ft_strnew(size);
	return (map);
}

void	place_tetri(char **map, t_tetri *tetri, int x, int y)
{
	int i;
	int c;
	int l;

	i = 1;
	while (i < 5)
	{
		c = tetri->x[i];
		l = tetri->y[i];
		map[y + l][x + c] = tetri->letter;
		i++;
	}
}

void	erase_tetri(char **map, t_tetri *tetri, int size)
{
	int j;
	int k;

	j = 0;
	k = 0;
	while (j < size)
	{
		k = 0;
		while (map[j][k])
		{
			if (map[j][k] == tetri->letter)
				map[j][k] = '.';
			k++;
		}
		j++;
	}
}

void	print_and_free(char **map, int size, t_tetri *begin_list)
{
	int i;

	i = 0;
	while (i < size)
	{
		ft_putendl(map[i]);
		ft_strdel(&map[i]);
		i++;
	}
	ft_strdel(&map[i]);
	free(map);
	del_lst_tetri(begin_list);
	exit(1);
}
