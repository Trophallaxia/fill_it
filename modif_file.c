/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_tetra.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sel-ahma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 15:11:35 by sel-ahma          #+#    #+#             */
/*   Updated: 2018/12/13 14:25:23 by sel-ahma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

char	**modif_file(char **tab, char *str)
{
	int		i;
	int		row;
	int		col;
	char	*alpha;

	alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	row = 0;
	i = 0;
	while (row < check_filepattern_count_blocks(str) * 4)
	{
		col = 0;
		while (col < 4)
		{
			if (tab[row][col] == '#')
				tab[row][col] = alpha[i];
			col++;
		}
		if (row % 4 == 3)
			i++;
		row++;
	}
	return (tab);
}
