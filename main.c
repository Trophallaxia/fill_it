/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 20:26:48 by sabonifa          #+#    #+#             */
/*   Updated: 2018/12/13 14:25:39 by sel-ahma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void	print_error(void)
{
	write(1, "error\n", 6);
}

void	go_fill_it(char *str)
{
	char	**tab;
	int		nb_blocks;

	tab = modif_file(ft_strsplit(str, '\n'), str);
	nb_blocks = check_filepattern_count_blocks(str);
	free(str);
	fill_it(tab, nb_blocks);
}

int		main(int ac, char **av)
{
	char	*str;
	int		error;

	error = 0;
	if (ac == 2)
	{
		if (!(str = read_file(av[1])))
		{
			print_error();
			return (0);
		}
		error = check_file(str);
		if (error > 0)
		{
			ft_strdel(&str);
			print_error();
		}
		else
			go_fill_it(str);
	}
	else
		write(1, "usage: fillit file_path\n", 24);
	return (0);
}
